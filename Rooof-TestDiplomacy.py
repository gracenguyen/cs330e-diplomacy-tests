from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_diplomacy_solve_1(self):
        r = StringIO(
            "A Madrid Move London\nB London Support C\nC Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Madrid\n")

    def test_diplomacy_solve_2(self):
        r = StringIO(
            "A Madrid Support C\nB Barcelona Hold\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Barcelona\n")

    def test_diplomacy_solve_3(self):
        r = StringIO(
            "A Barcelona Move Madrid\nB Madrid Hold\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")


if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          86      1     48      1    99%   59->63, 63
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                107      1     48      1    99%
"""
